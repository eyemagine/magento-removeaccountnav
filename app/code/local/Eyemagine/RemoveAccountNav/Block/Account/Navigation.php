<?php
/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * RemoveAccountNav
 *
 * @author EYEMAGINE <magento@eyemaginetech.com>
 * @category Eyemagine
 * @package Eyemagine_RemoveAccountNav
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

class Eyemagine_RemoveAccountNav_Block_Account_Navigation
    extends Mage_Customer_Block_Account_Navigation
{
    public function removeLink($name)
    {
        if (isset($this->_links[$name])) {
            unset($this->_links[$name]);
        }
        return $this;
    }   
}