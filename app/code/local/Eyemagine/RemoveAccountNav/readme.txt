/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * RemoveAccountNav
 *
 * @author EYEMAGINE <magento@eyemaginetech.com>
 * @category Eyemagine
 * @package Eyemagine_RemoveAccountNav
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

-------------------------------------------------------------------------------
DESCRIPTION:
-------------------------------------------------------------------------------

Allows you to remove account navigation links from layout update

ie:
    <layoutUpdates>
        <customer_account>
            <reference name="customer_account_navigation">
                <action method="removeLink"><name>tags</name></action>
                <action method="removeLink"><name>billing_agreements</name></action>
                <action method="removeLink"><name>recurring_profiles</name></action>
            </reference>
        </customer_account>
    </layoutUpdates>

Module Files:

  - app/etc/modules/RemoveAccountNav.xml
  - app/code/local/Eyemagine/RemoveAccountNav/*


-------------------------------------------------------------------------------
COMPATIBILITY:
-------------------------------------------------------------------------------

  - Magento Enterprise Edition 1.12.0.2
  

-------------------------------------------------------------------------------
RELEASE NOTES:
-------------------------------------------------------------------------------
    
v0.1.0: March 19th, 2013
  - Initial release